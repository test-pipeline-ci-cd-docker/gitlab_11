all: fattoriale test

test: test.o funzioni.o
	g++ -o test funzioni.o test.o

fattoriale: funzioni.o main.o
	g++ -o fattoriale funzioni.o main.o

main.o: funzioni.h main.cpp
	g++ -c main.cpp

funzioni.o: funzioni.cpp funzioni.h
	g++ -c funzioni.cpp funzioni.h

test.o: test.cpp funzioni.h
	g++ -c test.cpp