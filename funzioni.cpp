int fattoriale (int n) {
    int i, p;
    p = 1;
    for (i = 2; i <= n; i++) {
        p = p * i;
    }

    return p;
}

